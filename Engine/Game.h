#pragma once
#include "VectorTypes.h"

#include "OSAPI.h"

#include "MapInfo.h"


#define GAME_FLAGS_1_01_lightmap_related                 0x01
#define GAME_FLAGS_1_DRAW_BLV_DEBUGS    0x08
#define GAME_FLAGS_2_SATURATE_LIGHTMAPS 0x02
#define GAME_FLAGS_2_ALTER_GRAVITY      0x08
#define GAME_FLAGS_2_TARGETING_MODE     0x10
#define GAME_FLAGS_2_DRAW_BLOODSPLATS   0x20



/*  320 */
enum GAME_STATE
{
  GAME_STATE_PLAYING = 0,
  GAME_FINISHED = 1,
  GAME_STATE_CHANGE_LOCATION = 2,
  GAME_STATE_LOADING_GAME = 3,
  GAME_STATE_NEWGAME_OUT_GAMEMENU = 4,
  GAME_STATE_5 = 5,
  GAME_STATE_STARTING_NEW_GAME = 6,
  GAME_STATE_GAME_QUITTING_TO_MAIN_MENU = 7,
  GAME_STATE_PARTY_DIED = 8,
  GAME_STATE_FINAL_WINDOW = 9,
  GAME_STATE_A = 10
};



/*  105 */
#pragma pack(push, 1)
struct Game__StationaryLight
{
  Vec3_float_ vPosition;
  Vec3_float_ vRGBColor;
  float flt_18;
};
#pragma pack(pop)


/*  108 */
#pragma pack(push, 1)
struct Game_stru0
{
  int field_0;
  unsigned __int8 *ptr_4;
  int field_8;
  int field_C;
};
#pragma pack(pop)


/*  279 */
#pragma pack(push, 1)
//Game_stru1
struct Game_Bloodsplat
{
  float x;
  float y;
  float z;
  float r;
  float g;
  float b;
  float radius;
};
#pragma pack(pop)


class Vis;
class LightmapBuilder;
class ParticleEngine;
class Mouse;
class Keyboard;
class ThreadWard;
class CShow;
class GammaController;
struct stru9;
struct stru10;

/*  104 */
#pragma pack(push, 1)
struct Game
{
  static Game *Create();
  static void  Destroy();

protected: Game();
protected: virtual ~Game();


public:
  void _44E904();
  bool InitializeGammaController();
  bool PickMouse(float fPickDepth, unsigned int uMouseX, unsigned int uMouseY, bool bOutline, struct Vis_SelectionFilter *sprite_filter, struct Vis_SelectionFilter *face_filter);
  bool PickKeyboard(bool bOutline, struct Vis_SelectionFilter *sprite_filter, struct Vis_SelectionFilter *face_filter);
  void OutlineSelection();
  signed int _44EC23(struct Polygon *a2, int *a3, signed int a4);
  signed int _44ED0A(struct BLVFace *a2, int *a3, signed int a4);
  bool AlterGamma_BLV(struct BLVFace *pFace, signed int *pColor);
  bool AlterGamma_ODM(struct ODMFace *pFace, signed int *pColor);
  bool draw_debug_outlines();
  bool _44EEA7();
  bool _44F07B();
  void ToggleFlags(unsigned int uMask);
  void ToggleFlags2(unsigned int uFlag);
  void _44F0FD();
  void PushStationaryLights(int a2);
  void PrepareBloodsplats();
  void Deinitialize();
  void Loop();
  void DrawParticles();
  void Draw();

  //----- (0042EB6A) --------------------------------------------------------
  struct stru6 *GetStru6() {return this->pStru6Instance;}
  //----- (0042EB71) --------------------------------------------------------
  struct IndoorCameraD3D *GetIndoorCamera() {return this->pIndoorCameraD3D;}


  //void ( ***vdestructor_ptr)(Game *, bool);
  Game__StationaryLight pStationaryLights[25];
  char field_2C0[1092];
  unsigned int uNumStationaryLights;
  Game_Bloodsplat pBloodsplats[20];
  int field_938;
  int field_93C;
  int field_940;
  int field_944;
  int field_948;
  int field_94C;
  int field_950;
  int field_954;
  int field_958;
  int field_95C;
  int field_960;
  int field_964;
  int field_968;
  int field_96C;
  int field_970;
  Mouse *pMouse;
  int field_978;
  Game_stru0 stru_97C;
  char field_98C[1148];
  int uNumBloodsplats;
  int field_E0C;
  __int64 field_E10;
  int uNumStationaryLights_in_pStationaryLightsStack;
  unsigned int bGammaControlInitialized;
  unsigned int uFlags;
  unsigned int uFlags2;
  float fSaturation;
  unsigned __int64 uSomeGammaStartTime;
  __int64 uSomeGammaDeltaTime;
  void/*ThreadWard*/ *pThreadWardInstance;
  ParticleEngine *pParticleEngine;
  Mouse *pMouseInstance;
  LightmapBuilder *pLightmapBuilder;
  Vis *pVisInstance;
  stru6 *pStru6Instance;
  IndoorCameraD3D *pIndoorCameraD3D;
  stru9 *pStru9Instance;
  stru10 *pStru10Instance;
  void/*stru11*/ *pStru11Instance;
  void/*stru12*/ *pStru12Instance;
  void/*CShow*/ *pCShow;
  Keyboard *pKeyboardInstance;
  GammaController *pGammaController;
  int field_E74;
};
#pragma pack(pop)






extern Game *pGame;

void sub_42FBDD();
void CloseWindowBackground();
void GameUI_MsgProc();
void back_to_game();
void GUI_MainMenuMessageProc();

void UpdateUserInput_and_MapSpecificStuff();
void PrepareWorld(unsigned int _0_box_loading_1_fullscreen);
void DoPrepareWorld(unsigned int bLoading, int _1_fullscreen_loading_2_box);

//int __stdcall aWinProc(HWND hWnd, UINT Msg, WPARAM wParam, unsigned int lParam);
int __stdcall InsertMM7CDDialogFunc(HWND hDlg, int a2, __int16 a3, int a4);
bool __fastcall FindMM7CD(HWND hWnd, char *pCDDrive);
//bool __fastcall Initialize(HINSTANCE hInst, char *pCmdLine);

void Game_DeinitializeAndTerminate(int exitCode); // idb
void FinalInitialization();
bool __fastcall CheckMM7CD(char c);
void SecondaryInitialization();


void MM6_Initialize(const wchar_t *pIniFilename);
void MM7Initialization();

void PrepareToLoadODM(unsigned int bLoading, struct ODMRenderParams *a2);
void MainMenu_Loop();
unsigned int GameOverMenu(void *ecx0);
void ResetCursor_Palettes_LODs_Level_Audio_SFT_Windows();
void _461103_load_level_sub();
void  InitializeTurnBasedAnimations(void *);
void SetCurrentMenuID(enum MENU_STATE); // idb
enum MENU_STATE GetCurrentMenuID();
unsigned int GetGravityStrength();
void  GameUI_StatusBar_UpdateTimedString(unsigned int bForceHide); // idb

void __fastcall sub_44861E_set_texture(unsigned int uFaceCog, const char *pFilename);
void __fastcall sub_44892E_set_faces_bit(int sCogNumber, int bit, int on);
void __fastcall SetDecorationSprite(uint16_t uCog, bool bHide, const char *pFileName); // idb
void _494035_timed_effects__water_walking_damage__etc();
void _493938_regenerate(); 
void sub_491E3A();
const char *GetReputationString(signed int a1);
unsigned int __fastcall _494820_training_time(unsigned int a1);
void LoadLevel_InitializeLevelStr();
void OnMapLeave();
void OnMapLoad();
void Level_LoadEvtAndStr(const char *pLevelName);
void sub_4452BB();
bool _44100D_should_alter_right_panel();
void Transition_StopSound_Autosave(const char *pMapName, MapStartPoint point); // sub_44987B idb

void __fastcall sub_4451A8_press_any_key(int a1, int a2, int a4);

void OnTimer(int);
bool TeleportToNWCDungeon();