#include "Camera.h"


Camera *camera = new Camera();

float4x4 Camera::GetViewOrientation()
{
    float4x4 res;

    res[0].x = right.x;  res[0].y = up.x;  res[0].z = direction.x;  res[0].w = 0;
    res[1].x = right.y;  res[1].y = up.y;  res[1].z = direction.y;  res[1].w = 0;
    res[2].x = right.z;  res[2].y = up.z;  res[2].z = direction.z;  res[2].w = 0;
    /*res[0].x = right.x;      res[0].y = right.y;      res[0].z = right.z;      res[0].w = 0;
    res[1].x = up.x;         res[1].y = up.y;         res[1].z = up.z;         res[1].w = 0;
    res[2].x = direction.x;  res[2].y = direction.y;  res[2].z = direction.z;  res[2].w = 0;*/
    res[3].x = 0;            res[3].y = 0;            res[3].z = 0;            res[3].w = 1;

    return res;
}


/*void Camera::Initialize(const Vec3<float> *pPosition, const Quaternion *pOrientation)
{
 vPosition = *pPosition;
 pOrientation->ToMatrix(&mOrientation);
 //SetOrientation(pOrientation);
}*/


/*
unsigned int Camera::SetOrientation(const Quaternion *qNewOrientation)
{
 qOrientation = *qNewOrientation;

 vDirection = qOrientation.Transform(Vec3(0, 0, 1));
 vRight = qOrientation.Transform(Vec3(1, 0, 0));
 vUp = qOrientation.Transform(Vec3(0, 1, 0));

//  view matrix:

//dir = normal(At - Eye)
//right = normal(cross(Up, dir))
//newup = cross(dir, right)

// right.x           newup.x           dir.x          0
// right.y           newup.y           dir.y          0
// right.z           newup.z           dir.z          0
//-dot(right, eye)  -dot(newup, eye)  -dot(dir, eye)  1
 mView[0][0] = vRight.x;                mView[0][1] = vUp.x;                mView[0][2] = vDirection.x;                mView[1][3] = 0;
 mView[1][0] = vRight.y;                mView[1][1] = vUp.y;                mView[1][2] = vDirection.y;                mView[0][3] = 0;
 mView[2][0] = vRight.z;                mView[2][1] = vUp.z;                mView[2][2] = vDirection.z;                mView[2][3] = 0;
 mView[3][0] = -Dot(vRight, vPosition); mView[3][1] = -Dot(vUp, vPosition); mView[3][2] = -Dot(vDirection, vPosition); mView[3][3] = 1;

 return 0;
}*/

void Camera::Move(float amount)
{
    position += direction * amount;
}

void Camera::RotateX(float angle)
{
    fRotationX += angle;
    if (fRotationX >= pi_double * 0.5 - 0.01)
    {
        angle -= fRotationX - (pi_double * 0.5f - 0.01);
        fRotationX = 3.141519 * 0.5f - 0.01;
    }
    else if (fRotationX < -3.14159 * 0.5 + 0.01)
    {
        angle += fRotationX - (-pi_double * 0.5f + 0.01);
        fRotationX = -pi_double * 0.5f + 0.01;
    }

    auto orientation = float4x4::RotationX(angle) * GetViewOrientation();

    /*direction = orientation.Transofrm(float3(0, 0, 1));
    up = orientation.Transofrm(float3(0, 1, 0));
    right = orientation.Transofrm(float3(1, 0, 0));*/
    direction = float3(orientation[0].z, orientation[1].z, orientation[2].z);
    up = float3(orientation[0].y, orientation[1].y, orientation[2].y);
    right = float3(orientation[0].x, orientation[1].x, orientation[2].x);
}

void Camera::RotateY(float angle)
{
    auto orientation = float4x4::RotationY(angle) * GetViewOrientation();

    //direction = orientation.Transofrm(float3(0, 0, 1));
    //up = orientation.Transofrm(float3(0, 1, 0));
    //right = orientation.Transofrm(float3(1, 0, 0));
    direction = float3(orientation[0].z, orientation[1].z, orientation[2].z);
    up = float3(orientation[0].y, orientation[1].y, orientation[2].y);
    right = float3(orientation[0].x, orientation[1].x, orientation[2].x);
}

void Camera::Rotate(const float3 &axis, float angle)
{
    Quaternion qRotation;
    qRotation.Rotate(axis, angle);

    auto orientation = qRotation.ToMatrix() * GetViewOrientation();

    direction = orientation.Transofrm(float3(0, 0, 1));
    up = orientation.Transofrm(float3(0, 1, 0));
    right = orientation.Transofrm(float3(1, 0, 0));
}

void Camera::SetDirection(const float3 &new_direction)
{
    direction = new_direction.Normalized();

    qOrientation.Rotate(Cross(float3(0, 0, 1), direction), direction.z); // (1 0 0) cross dir, (1 0 0) dot dir
    auto orientation = qOrientation.ToMatrix();

    direction = orientation.Transofrm(float3(0, 0, 1));
    up = orientation.Transofrm(float3(0, 1, 0));
    right = orientation.Transofrm(float3(1, 0, 0));
}


#include <Windows.h>
float4x4 pro;
float4x4 vie;
HMODULE d3dx = LoadLibraryA("d3dx9_42.dll");

void Camera::SetProjection(float fovy, unsigned int viewport_width, unsigned int viewport_height, float znear, float zfar)
{
    this->znear = znear;
    this->zfar = zfar;
    this->fov_y = fovy;
    this->aspect = (float)viewport_width / (float)viewport_height;

/*
      //D3DXMatrixPerspectiveFovLH 
      //xScale     0          0               0
      //0        yScale       0               0
      //0          0       zf/(zf-zn)         1
      //0          0       -zn*zf/(zf-zn)     0
      //
      // yScale = ctg(fovY/2)
      // xScale = yScale / aspect ratio     * -1 for right-handed
      // ctg(x) = 1/tg(x) = tg(pi/2 - x)
      // tg(x) = sin(x) / cos(x)
*/

    float yScale = tan((pi_double - fovy) * 0.5f);
    float xScale = yScale / aspect;
    float zdif = zfar - znear;

    mProjection.m[0].x = xScale; mProjection.m[0].y = 0;      mProjection.m[0].z = 0;                    mProjection.m[0].w = 0;
    mProjection.m[1].x = 0;      mProjection.m[1].y = yScale; mProjection.m[1].z = 0;                    mProjection.m[1].w = 0;
    mProjection.m[2].x = 0;      mProjection.m[2].y = 0;      mProjection.m[2].z = zfar / zdif;          mProjection.m[2].w = 1;
    mProjection.m[3].x = 0;      mProjection.m[3].y = 0;      mProjection.m[3].z = -znear * zfar / zdif; mProjection.m[3].w = 0;

    auto d3dxperspective = (void (__stdcall *)(float4x4 *, float, float, float, float))GetProcAddress(d3dx, "D3DXMatrixPerspectiveFovLH");
    d3dxperspective(&pro, fovy, aspect, znear, zfar);
}

float4x4 Camera::GetProjection()
{
    return mProjection;
}

float4x4 Camera::GetViewProjection()
{
    auto d3dxlookat = (void (__stdcall *)(float4x4 *, float *, float *, float *))GetProcAddress(d3dx, "D3DXMatrixLookAtLH");

    float eye[] = {position.x, position.y, position.z};
    float at[] = {position.x + direction.x, position.y + direction.y, position.z + direction.z};
    float upv[] = {up.x, up.y, up.z};
    d3dxlookat(&vie, eye, at, upv);

    
    float4x4 vp;
    auto d3dxmult = (void (__stdcall *)(float4x4 *, float4x4 *, float4x4 *))GetProcAddress(d3dx, "D3DXMatrixMultiply");
    d3dxmult(&vp, &vie, &pro);



    float4x4 view;

    view[0].x = right.x;                view[0].y = up.x;                view[0].z = direction.x;                view[1].w = 0;
    view[1].x = right.y;                view[1].y = up.y;                view[1].z = direction.y;                view[0].w = 0;
    view[2].x = right.z;                view[2].y = up.z;                view[2].z = direction.z;                view[2].w = 0;
    view[3].x = -Dot(right, position);  view[3].y = -Dot(up, position);  view[3].z = -Dot(direction, position);  view[3].w = 1;
    
    float4x4 myvp = view * mProjection;
    return view * mProjection;
}



void Camera::SetPosition(const float3 &position)
{
    this->position = position;

    //view[3][0] = -Dot<float>(vRight, vPosition); mView[3][1] = -Dot<float>(vUp, vPosition); mView[3][2] = -Dot<float>(vDirection, vPosition);
}