#pragma once
#include "Math.inl"

class Camera
{
    public:
        inline Camera()
        {
            qOrientation.Identity();
            
            position = float3(0, 0, 0);
            direction = float3(0, 0, 1);
            up = float3(0, 1, 0);
            right = float3(1, 0, 0);

            fRotationX = 0;
            fRotationY = 0;
   }

   //void Initialize(const float3 &pPosition, const Quaternion *pOrientation);

   void     SetProjection(float fovy, unsigned int viewport_width, unsigned int viewport_height, float znear, float zfar);
   float4x4 GetViewOrientation();  // view lacking position info
   float4x4 GetView();             // complete view matrix
   float4x4 GetProjection();
   float4x4 GetViewProjection();

   void   SetPosition(const float3 &);
   float3 GetPosition() { return position; }

   void   SetDirection(const float3 &);
   float3 GetDirection() { return direction; }

   void   Move(float);
   void   RotateX(float angle);
   void   RotateY(float angle);
   void   Rotate(const float3 &axis, float angle);

 protected:
   float3 position;
   float3 direction,
          up,
          right;
   Quaternion qOrientation;

   float4x4 mProjection;
   float znear, zfar;
   float fov_y, aspect;

   float4x4 mViewProjection;

   float fRotationX;
   float fRotationY;
};

extern Camera *camera;