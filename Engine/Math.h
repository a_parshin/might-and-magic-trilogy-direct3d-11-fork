#pragma once
#include <math.h>

struct float3;
struct float4;

inline float  Dot(const float3 &lhs, const float3 &rhs);
inline float  Dot(const float4 &lhs, const float4 &rhs);
inline float3 Cross(const float3 &v1, const float3 &v2);

#define pi_double 3.141592653589793

struct float3
{
    inline float3(): x(0.0f), y(0.0f), z(0.0f) {}
    inline float3(float x, float y, float z): x(x), y(y), z(z) {}

    inline float Length() const
    {
        return sqrtf(Dot(*this, *this));
    }
    inline float3 Normalized() const
    {
        float dot = Dot(*this, *this);
        if (dot < 0 || dot > 0)
        {
            float inv = 1.0f / sqrtf(dot);
            return float3(x * inv, y * inv, z * inv);
        }
        else return float3();
    }

    
    // ------------------- operators ----------------------
    inline float3 &operator =(const float3 &rhs)
    {
        x = rhs.x, y = rhs.y, z = rhs.z;
        return *this;
    }

    inline float3 &operator +=(const float3 &rhs)
    {
        x += rhs.x, y += rhs.y, z += rhs.z;
        return *this;
    }
    friend float3 operator +(float3 lhs, const float3 &rhs) { return lhs += rhs; }

    inline float3 &operator -=(const float3 &rhs)
    {
        x -= rhs.x, y -= rhs.y, z -= rhs.z;
        return *this;
    }
    friend float3 operator -(float3 lhs, const float3 &rhs) { return lhs -= rhs; }

    friend float3 operator *(float3 lhs, float s)
    {
        return float3(lhs.x * s, lhs.y * s, lhs.z * s);
    }
    

    float x, y, z;
};


struct float4
{
    inline float4(): x(0.0f), y(0.0f), z(0.0f), w(0.0f) {}
    inline float4(float x, float y, float z, float w): x(x), y(y), z(z), w(w) {}

    inline float Length() const
    {
        return sqrtf(Dot(*this, *this));
    }
    inline float4 Normalized() const
    {
        float dot = Dot(*this, *this);
        if (dot < 0 || dot > 0)
        {
            float inv = 1.0f / sqrtf(dot);
            return float4(x * inv, y * inv, z * inv, w * inv);
        }
        else return float4();
    }

    
    // ------------------- operators ----------------------
    inline float4 &operator =(const float4 &rhs)
    {
        x = rhs.x, y = rhs.y, z = rhs.z, w = rhs.w;
        return *this;
    }

    inline float4 &operator +=(const float4 &rhs)
    {
        x += rhs.x, y += rhs.y, z += rhs.z, w += rhs.w;
        return *this;
    }
    friend float4 operator +(float4 lhs, const float4 &rhs) { return lhs += rhs; }

    inline float4 &operator -=(const float4 &rhs)
    {
        x -= rhs.x, y -= rhs.y, z -= rhs.z, w -= rhs.w;
        return *this;
    }
    friend float4 operator -(float4 lhs, const float4 &rhs) { return lhs -= rhs; }


    float x, y, z, w;
};









struct float4x4;
struct Quaternion
{
 inline Quaternion() {}
 inline Quaternion(float s, float x, float y, float z): s(s), x(x), y(y), z(z) {}

 inline void        Identity();
 inline Quaternion &Rotate(const float3 &axis, float angle);
 inline void        Rotate(float lattitude, float longitude);
 inline float4x4    ToMatrix() const;

 inline Quaternion  Conjugate();
 inline float3      Transform(const float3 &v);
 inline float3      Transform(float x, float y, float z);

 inline Quaternion  operator *(const Quaternion &q) const;
 inline Quaternion &operator =(const Quaternion &q);

 float s, x, y, z;
};



struct float4x4
{
    inline float4 &operator [](unsigned int row)
    {
        return m[row];
    }

    inline float4x4  operator *(float4x4 &);
    inline float4x4 &operator =(float4x4 &rhs)
    {
        m[0] = rhs[0], m[1] = rhs[1], m[2] = rhs[2], m[3] = rhs[3];
        return *this;
    }

    inline float3 Transofrm(const float3 &);

    inline static float4x4 Identity();
    inline static float4x4 RotationX(float angle);
    inline static float4x4 RotationY(float angle);
    inline static float4x4 RotationZ(float angle);

    float4 m[4];
};