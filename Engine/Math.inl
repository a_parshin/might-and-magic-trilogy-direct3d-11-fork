#include "Math.h"


inline float Dot(const float3 &lhs, const float3 &rhs)
{
    return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
}

inline float Dot(const float4 &lhs, const float4 &rhs)
{
    return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z + lhs.w * rhs.w;
}


inline float3 Cross(const float3 &v1, const float3 &v2)
{
 return float3(v1.y * v2.z - v1.z * v2.y,
               v1.z * v2.x - v1.x * v2.z,
               v1.x * v2.y - v1.y * v2.x);
}








inline Quaternion &Quaternion::Rotate(const float3 &axis, float angle)
{
 s = cosf(angle * 0.5f);
 x = axis.x * sinf(angle * 0.5f);
 y = axis.y * sinf(angle * 0.5f);
 z = axis.z * sinf(angle * 0.5f);

 return *this;
}

inline void Quaternion::Identity()
{
 s = 1;
 x = y = z = 0;
}

inline void Quaternion::Rotate(float lattitude, float longitude)
{
 // � = lattitude from [0; �)       � runs from (1, 0, 0) to (-1, 0, 0) in orthogonal coordinate system
 // o = longitude from [0; 2�)      o runs in the north pole direction
 // (0, 0) spherical  =  (1, 0, 0) orthogonal

 // let vp be normalized projection of desired vector onto XZ plane, vp = (cos�, 0, sin�)
 // then we need to rotate from vp for o radians
 // axis of rotation can be found by : axis = vp cross n, where n is the plane's normal; n = Y = (0, 1, 0) for XZ plane

 // v =
   // cosf(lattitude) * cosf(longitude)
   // sinf(longitude)
   // sinf(lattitude) * cosf(longitude)
 // X cross v = (0, -v.z, v.y)        v cross X = (0, v.z, -v.y)
 // X dot v = v.x


 // next we need to find roation axis and angle between v and (1 0 0)
 float angle = acosf(cosf(lattitude) * cosf(longitude));
 float3 axis = float3(0.0f,
                      -sinf(lattitude) * cosf(longitude),
                      sinf(longitude));

 Rotate(axis.Normalized(), angle);
}

inline Quaternion Quaternion::operator *(const Quaternion &q) const
{
 //q = [s, v], |v| = 1
 //q * q' = (s * s' - v dot v', v cross v' + sv' + s'v)

 return Quaternion(s * q.s - (x * q.x + y * q.y + z * q.z),
                   y * q.z - z * q.y + s * q.x + q.s * x,
                   z * q.x - x * q.z + s * q.y + q.s * y,
                   x * q.y - y * q.x + s * q.z + q.s * z);
}

inline Quaternion Quaternion::Conjugate()
{
 return Quaternion(s, -x, -y, -z);
}


inline float3 Quaternion::Transform(const float3 &v)
{
 //]p = [0, v]:
 // v' = q * p * q^-1  , where v' is v roatated by q
 Quaternion qVec(0, v.x, v.y, v.z);

 qVec = *this * qVec * Conjugate();

 return float3(qVec.x, qVec.y, qVec.z);
}

inline Quaternion &Quaternion::operator =(const Quaternion &q)
{
 s = q.s;
 x = q.x;
 y = q.y;
 z = q.z;
 return *this;
}

inline float4x4 Quaternion::ToMatrix() const
{
    float4x4 res;

    res[0].x = 1 - 2 * (y * y + z * z);
    res[0].y = 2 * (x * y + z * s);
    res[0].z = 2 * (x * z - y * s);
    res[0].w = 0;

    res[1].x = 2 * (x * y - z * s);
    res[1].y = 1 - 2 * (x * x + z * z);
    res[1].z = 2 * (z * y + x * s);
    res[1].w = 0;

    res[2].x = 2 * (x * z + y * s);
    res[2].y = 2 * (y * z - x * s);
    res[2].z = 1 - 2 * (x * x + y * y);
    res[2].w = 0;

    res[3].x = 0;
    res[3].y = 0;
    res[3].z = 0;
    res[3].w = 1;

    return res;
}






inline float4x4 float4x4::Identity()
{
    float4x4 res;

    res[0].x = 1;    res[0].y = 0;   res[0].z = 0;   res[0].w = 0;
    res[1].x = 0;    res[1].y = 1;   res[1].z = 0;   res[1].w = 0;
    res[2].x = 0;    res[2].y = 0;   res[2].z = 1;   res[2].w = 0;
    res[3].x = 0;    res[3].y = 0;   res[3].z = 0;   res[3].w = 1;

    return res;
}

inline float4x4 float4x4::RotationX(float a)
{
    float4x4 res;

    res[0].x = 1;    res[0].y = 0;         res[0].z = 0;         res[0].w = 0;
    res[1].x = 0;    res[1].y = cosf(a);   res[1].z = sinf(a);   res[1].w = 0;
    res[2].x = 0;    res[2].y = sinf(a);   res[2].z = cosf(a);   res[2].w = 0;
    res[3].x = 0;    res[3].y = 0;         res[3].z = 0;         res[3].w = 1;

    if (true) // counter-clockwise
        res[1].z *= -1;
    else      // clockwise
        res[2].y *= -1;
    
    return res;
}



inline float4x4 float4x4::RotationY(float a)
{
    float4x4 res;

    res[0].x = cosf(a);    res[0].y = 0;   res[0].z = sinf(a);   res[0].w = 0;
    res[1].x = 0;          res[1].y = 1;   res[1].z = 0;         res[1].w = 0;
    res[2].x = sinf(a);    res[2].y = 0;   res[2].z = cosf(a);   res[2].w = 0;
    res[3].x = 0;          res[3].y = 0;   res[3].z = 0;         res[3].w = 1;
    
    if (true) // counter-clockwise
        res[2].x *= -1;
    else      // clockwise
        res[0].z *= -1;

    return res;
}





inline float4x4 float4x4::operator *(float4x4 &rv)
{
    float4x4 res;

    for (int i = 0; i < 4; ++i)
    {
        res[i].x = m[i].x * rv[0].x + m[i].y * rv[1].x + m[i].z * rv[2].x + m[i].w * rv[3].x;
        res[i].y = m[i].x * rv[0].y + m[i].y * rv[1].y + m[i].z * rv[2].y + m[i].w * rv[3].y;
        res[i].z = m[i].x * rv[0].z + m[i].y * rv[1].z + m[i].z * rv[2].z + m[i].w * rv[3].z;
        res[i].w = m[i].x * rv[0].w + m[i].y * rv[1].w + m[i].z * rv[2].w + m[i].w * rv[3].w;
    }
    
    return res;
}


inline float3 float4x4::Transofrm(const float3 &v)
{
    float4 res = float4(v.x * m[0].x + v.y * m[1].x + v.z * m[2].x + m[3].x,
                        v.x * m[0].y + v.y * m[1].y + v.z * m[2].y + m[3].y,
                        v.x * m[0].z + v.y * m[1].z + v.z * m[2].z + m[3].z,
                        v.x * m[0].w + v.y * m[1].w + v.z * m[2].w + m[3].w);

    return float3(res.x / res.w, res.y / res.w, res.z / res.w);
}