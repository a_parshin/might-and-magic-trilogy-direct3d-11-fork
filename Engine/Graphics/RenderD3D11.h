#pragma once

#include <cstdint>
#include <cstdio>
#include <map>

#include <memory>
using std::tr1::shared_ptr;
using std::tr1::make_shared;

#include "lib\legacy_dx\d3d.h"
#include <d3d11.h>

#include "OSWindow.h"
#include "RenderStruct.h"

#include "../VectorTypes.h"

#include "IRender.h"



struct RenderD3D11: public IRender
{
  RenderD3D11();
  virtual ~RenderD3D11();

  static RenderD3D11 *Create() {return new RenderD3D11;}

  virtual bool Initialize(OSWindow *window);

  virtual void ClearBlack();
  virtual void PresentBlackScreen();

  virtual void SaveWinnersCertificate(const char *a1);
  virtual void ClearTarget(unsigned int uColor);
  virtual void Present();

  virtual void _49FD3A_fullscreen();
  virtual bool InitializeFullscreen();

  virtual void CreateZBuffer();
  virtual void Release();

  virtual bool SwitchToWindow();
  virtual void RasterLine2D(signed int uX, signed int uY, signed int uZ, signed int uW, unsigned __int16 uColor);
  virtual void ClearZBuffer(int a2, int a3);
  virtual void SetRasterClipRect(unsigned int uX, unsigned int uY, unsigned int uZ, unsigned int uW);
  virtual bool LockSurface_DDraw4(IDirectDrawSurface4 *pSurface, DDSURFACEDESC2 *pDesc, unsigned int uLockFlags);
  virtual void GetTargetPixelFormat(DDPIXELFORMAT *pOut);
  virtual void LockRenderSurface(void **pOutSurfacePtr, unsigned int *pOutPixelsPerRow);
  virtual void UnlockBackBuffer();
  virtual void LockFrontBuffer(void **pOutSurface, unsigned int *pOutPixelsPerRow);
  virtual void UnlockFrontBuffer();
  virtual void RestoreFrontBuffer();
  virtual void RestoreBackBuffer();
  virtual void BltToFront(RECT *pDstRect, IDirectDrawSurface *pSrcSurface, RECT *pSrcRect, unsigned int uBltFlags);
  virtual void BltBackToFontFast(int a2, int a3, RECT *pSrcRect);
  virtual void BeginSceneD3D();

  virtual unsigned int GetActorTintColor(float a2, int tint, int a4, int a5, RenderBillboard *a6);

  virtual void DrawPolygon(unsigned int uNumVertices, struct Polygon *a3, ODMFace *a4, IDirect3DTexture2 *pTexture);
  virtual void DrawTerrainPolygon(unsigned int uNumVertices, struct Polygon *a4, IDirect3DTexture2 *a5, bool transparent, bool clampAtTextureBorders);
  virtual void DrawIndoorPolygon(unsigned int uNumVertices, struct BLVFace *a3, IDirect3DTexture2 *pHwTex, struct Texture *pTex, int uPackedID, unsigned int uColor, int a8);

  virtual void MakeParticleBillboardAndPush_BLV(RenderBillboardTransform_local0 *a2, IDirect3DTexture2 *a3, unsigned int uDiffuse, int angle);
  virtual void MakeParticleBillboardAndPush_ODM(RenderBillboardTransform_local0 *a2, IDirect3DTexture2 *a3, unsigned int uDiffuse, int angle);

  virtual void DrawBillboards_And_MaybeRenderSpecialEffects_And_EndScene();
  virtual void DrawBillboard_Indoor(RenderBillboardTransform_local0 *pSoftBillboard, Sprite *pSprite, int dimming_level);
  virtual void _4A4CC9_AddSomeBillboard(struct stru6_stru1_indoor_sw_billboard *a1, int diffuse);
  virtual void TransformBillboardsAndSetPalettesODM();
  virtual void DrawBillboardList_BLV();

  virtual void DrawProjectile(float srcX, float srcY, float a3, float a4, float dstX, float dstY, float a7, float a8, IDirect3DTexture2 *a9);
  virtual bool LoadTexture(const char *pName, unsigned int bMipMaps, IDirectDrawSurface4 **pOutSurface, IDirect3DTexture2 **pOutTexture);
  virtual bool MoveSpriteToDevice(Sprite *pSprite);

  virtual void BeginScene();
  virtual void EndScene();
  virtual void ScreenFade(unsigned int color, float t);

  virtual void SetUIClipRect(unsigned int uX, unsigned int uY, unsigned int uZ, unsigned int uW);
  virtual void ResetUIClipRect();
  virtual void CreditsTextureScroll(unsigned int pX, unsigned int pY, int move_X, int move_Y, RGBTexture *pTexture);
  
  virtual void DrawTextureNew(float u, float v, struct Texture *);
  virtual void DrawTextureNew(float u, float v, struct RGBTexture *);
  virtual void DrawTextureRGB(unsigned int uOutX, unsigned int uOutY, RGBTexture *a4);
  virtual void DrawTextureIndexed(signed int uX, signed int uY, struct Texture *tex);
  virtual void DrawTextureIndexedAlpha(unsigned int uX, unsigned int uY, struct Texture *pTexture);

  virtual void ZBuffer_Fill_2(signed int a2, signed int a3, struct Texture *pTexture, int a5);
  virtual void DrawMaskToZBuffer(signed int uOutX, unsigned int uOutY, struct Texture *pTexture, int zVal);
  virtual void DrawAura(unsigned int a2, unsigned int a3, struct Texture *a4, struct Texture *a5, int a6, int a7, int a8);
  virtual void _4A65CC(unsigned int x, unsigned int y, struct Texture *a4, struct Texture *a5, int a6, int a7, int a8);

  virtual void DrawTransparentRedShade(unsigned int a2, unsigned int a3, struct Texture *a4);
  virtual void DrawTransparentGreenShade(signed int a2, signed int a3, struct Texture *pTexture);
  virtual void DrawFansTransparent(const RenderVertexD3D3 *vertices, unsigned int num_vertices);

  virtual void DrawMasked(signed int a2, signed int a3, struct Texture *pTexture, unsigned __int16 mask);
  virtual void GetLeather(unsigned int a2, unsigned int a3, struct Texture *a4, __int16 height);

  virtual void DrawTextAlpha(int x, int y, unsigned char* font_pixels, int a5, unsigned int uFontHeight, unsigned __int16 *pPalette, bool present_time_transparency);
  virtual void DrawText(signed int uOutX, signed int uOutY, unsigned __int8 *pFontPixels, unsigned int uCharWidth, unsigned int uCharHeight, unsigned __int16 *pFontPalette, unsigned __int16 uFaceColor, unsigned __int16 uShadowColor);
  virtual void DrawTextNew(int *x, int *y, const char *string, int string_len, struct GUIFont *font, unsigned int face_color32);

  virtual void FillRectFast(unsigned int uX, unsigned int uY, unsigned int uWidth, unsigned int uHeight, unsigned int uColor16);
  virtual void _4A6DF5(unsigned __int16 *pBitmap, unsigned int uBitmapPitch, struct Vec2_int_ *pBitmapXY, void *pTarget, unsigned int uTargetPitch, Vec4_int_ *a7);
  virtual void DrawTranslucent(unsigned int a2, unsigned int a3, struct Texture *a4);

  virtual void DrawBuildingsD3D();

  virtual void DrawIndoorSky(unsigned int uNumVertices, unsigned int uFaceID);
  virtual void DrawIndoorSkyPolygon(signed int uNumVertices, struct Polygon *pSkyPolygon, IDirect3DTexture2 *pTexture);

  virtual void PrepareDecorationsRenderList_ODM();
  virtual void DrawSpriteObjects_ODM();

  virtual void RenderTerrainD3D();

  virtual void ChangeBetweenWinFullscreenModes();
  virtual bool AreRenderSurfacesOk();
  virtual bool IsGammaSupported();

  virtual void SaveScreenshot(const char *pFilename, unsigned int width, unsigned int height);
  virtual void PackScreenshot(unsigned int width, unsigned int height, void *out_data, unsigned int data_size, unsigned int *screenshot_size);
  virtual void SavePCXScreenshot();

  virtual int _46�6��_GetActorsInViewport(int pDepth);

  virtual void BeginLightmaps();
  virtual void EndLightmaps();  
  virtual void BeginLightmaps2();
  virtual void EndLightmaps2();
  virtual bool DrawLightmap(struct Lightmap *pLightmap, struct Vec3_float_ *pColorMult, float z_bias);

  virtual void BeginDecals();
  virtual void EndDecals();
  virtual void DrawDecal(struct Decal *pDecal, float z_bias);
  
  virtual void do_draw_debug_line_d3d(const RenderVertexD3D3 *pLineBegin, signed int sDiffuseBegin, const RenderVertexD3D3 *pLineEnd, signed int sDiffuseEnd, float z_stuff);
  virtual void DrawLines(const RenderVertexD3D3 *vertices, unsigned int num_vertices);

  virtual void DrawSpecialEffectsQuad(const RenderVertexD3D3 *vertices, IDirect3DTexture2 *texture);

  virtual void am_Blt_Copy(RECT *pSrcRect, POINT *pTargetXY, int a3);
  virtual void am_Blt_Chroma(RECT *pSrcRect, POINT *pTargetPoint, int a3, int blend_mode);
  
  struct D3D11Scene *scene;
    virtual IScene *GetScene();
    virtual void    DrawScene(Camera *);
    /*virtual SceneNode *CreateSkyboxNode(const char *name, const char *texture);
    
    virtual void RenderSceneGraph();
    void RenderNode(SceneNode *node);

    typedef void (RenderD3D11::*RenderHandler)(SceneNode *);
    RenderHandler OnNodeRender[256];
    void OnRootNodeRender(SceneNode *);
    void OnSkyboxNodeRender(SceneNode *);*/

  public:

    virtual void WritePixel16(int x, int y, unsigned __int16 color)
    {
__debugbreak();
    }

    virtual unsigned __int16 ReadPixel16(int x, int y)
    {
__debugbreak(); return 0;
    }

    virtual void ToggleTint()          {}
    virtual void ToggleColoredLights() {}

    virtual unsigned int GetRenderWidth() {return window->GetWidth();}
    virtual unsigned int GetRenderHeight() {return window->GetHeight();}

    virtual void Sub01();


  protected:
    void DrawTexture(float u, float v, int texture_width, int texture_height, ID3D11ShaderResourceView *srv, ID3D11BlendState *blend);

    OSWindow *window;

    IDXGISwapChain          *pSwapChain;
    ID3D11RenderTargetView  *primary_srv;
    ID3D11DepthStencilView  *default_depth_srv;
    ID3D11RasterizerState   *default_rasterizer;
    ID3D11DepthStencilState *default_depthstencil;
    ID3D11BlendState        *default_blend;
    D3D11_VIEWPORT           default_viewport;

    ID3D11VertexShader      *ui_vs;
    ID3D11PixelShader       *ui_ps;
    ID3D11Buffer            *ui_cb_fast;
    ID3D11InputLayout       *ui_layout;
    ID3D11Buffer            *ui_vb;
    ID3D11DepthStencilState *ui_depthstencil;
    ID3D11BlendState        *ui_blend_solid;
    ID3D11BlendState        *ui_blend_alpha;
    ID3D11RasterizerState   *ui_rasterizer;
    D3D11_RECT               ui_clip_rect;
    
    ID3D11VertexShader      *ui_text_vs;
    ID3D11Buffer            *ui_text_cb_fast;
    ID3D11PixelShader       *ui_text_ps;
    ID3D11InputLayout       *ui_text_layout;
    ID3D11Buffer            *ui_text_vb;

    
    RenderHWLContainer pD3DBitmaps;
    RenderHWLContainer pD3DSprites;

    struct DeviceTexture
    {
      D3D11_TEXTURE2D_DESC      desc;
      ID3D11ShaderResourceView *srv;
    };
    std::map<int, shared_ptr<DeviceTexture>> textures;
};
