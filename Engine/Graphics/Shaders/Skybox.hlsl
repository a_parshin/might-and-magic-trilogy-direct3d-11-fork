cbuffer slow_camera: register(b1)
{
    float    xscale        : packoffset(c0.x); // ctg(Y fov / 2) / aspect
    float    yscale        : packoffset(c0.y); // aspect ratio
    float    zfactor       : packoffset(c0.z); // zfar / (zfar - znear)
    float    wfactor       : packoffset(c0.w); // -znear * zfar / (zfar - znear)
    float4x4 viewproj      : packoffset(c1);   // camera's viewprojection
    float4x4 worldviewproj : packoffset(c5);   // camera's wvp
};

struct VInput
{
    float4 pos: POSITION0;
};

struct VOutput
{
    float4 pos: SV_Position0;
    float3 uv:  TEXCOORD0;
};

VOutput vs(VInput vin)
{
    VOutput vout;
    vout.pos = float4(vin.pos.xy, 1, 1);

    //float4 projection = float4(xscale, yscale, zfactor, wfactor);
    float3 view = normalize(float3(vin.pos.x, -vin.pos.y, 1));

    //float2 test_uv = vin.pos.xy * float2(0.5, -0.5) + float2(0.5, 0.5);
    float3 identity_view = normalize(float3(vin.pos.x, vin.pos.y, 1)); // form viewing rect around default (0, 0, 1) vector
    vout.uv = mul(float4(identity_view, 1), viewproj).xyz;             // rotate vewing vector according to camera and project

  /*vout.uv.x = dot(projection, float4(view.x, 0,      0,      1));
  vout.uv.y = dot(projection, float4(0,      view.y, 0,      1));
  vout.uv.z = dot(projection, float4(0,      0,      view.z, 1));*/

    return vout;
}


TextureCube<float4> env_map: register(t0);

SamplerState basic_sampler: register(s0);

float4 main(VOutput pin): SV_Target0
{
    return env_map.Sample(basic_sampler, pin.uv);
}