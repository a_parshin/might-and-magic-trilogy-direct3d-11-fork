cbuffer fast: register(b7)
{
    float4 color           : packoffset(c0);
    float4 positions[1024] : packoffset(c1);
    float4 metrics[1024]   : packoffset(c1025);
    //float4 colors[1024]  : packoffset(c1024);
};



struct VInput
{
    uint   id:  SV_VertexID;
};

struct VOutput
{
    float4 pos   : SV_Position0;
    float4 color : COLOR0;
    float2 uv    : TEXCOORD0;
};


VOutput vs(VInput vin)
{
    VOutput vout;

    //float2 normalized_pos = vin.pos.xy * position.xy + vin.pos.zw * size.xy;
    //vout.pos.xy = float2(2, -2) * normalized_pos + float2(-1, 1);
    //vout.pos.zw = float2(0, 1);
    //vout.pos = float4(vin.id, vin.id, 0, 1);

//       (x, y) = f(i) = [(i * 2) & 2, i & 2]
//    i  0  1  2  3  4  5  6  7  8  9 10 11
//    x  0  2  0  2  0  2  0  2  0  2  0  2
//    y  0  0  2  2  0  0  2  2  0  0  2  2

    uint id = vin.id;

    // triangle #1:  (0, 0), (1, 0), (0, 1)  or  i = 0, 1, 2
    // triangle #2:  (1, 0), (0, 1), (1, 1)  or  i = 1, 2, 3
    uint quad_id = id / 6;
    uint in_quad_id = id % 6;
    uint triangle_id = in_quad_id / 3;  // 0 or 1

    uint i = id % 3 + triangle_id;  // i = 0 1 2 or 1 2 3
    float2 uv = float2(((i << 1) & 2) >> 1, (i & 2) >> 1);

    float2 screen_position = positions[quad_id].xy;
    float2 screen_size = positions[quad_id].zw;
    float2 texture_position = metrics[quad_id].xy;
    float2 texture_size = metrics[quad_id].zw;

    float2 pos = screen_position + uv * screen_size;
    vout.pos.xy = float2(2, -2) * pos + float2(-1, 1);
    vout.pos.zw = float2(0, 1);
    vout.uv = texture_position + uv * texture_size;
    vout.color = color;
    return vout;
}




SamplerState      basic_sampler: register(s0);
Texture2D<float4> image: register(t0);

float4 main(VOutput pin): SV_Target0
{
    return pin.color * image.Sample(basic_sampler, pin.uv);
}