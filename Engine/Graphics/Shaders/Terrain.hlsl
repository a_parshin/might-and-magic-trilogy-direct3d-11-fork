//------------------------------------------------------------------------------------
// Constant Buffer Variables
//------------------------------------------------------------------------------------
cbuffer slow_camera: register(b1)
{
    float    xscale        : packoffset(c0.x); // ctg(Y fov / 2) / aspect
    float    yscale        : packoffset(c0.y); // aspect ratio
    float    zfactor       : packoffset(c0.z); // zfar / (zfar - znear)
    float    wfactor       : packoffset(c0.w); // -znear * zfar / (zfar - znear)
    float4x4 viewproj      : packoffset(c1);   // camera's viewprojection
    float4x4 worldviewproj : packoffset(c5);   // camera's wvp
};
//------------------------------------------------------------------------------------
struct VInput
{
	// ��, ��� ��������� ��������� ������
    int4 pos: POSITION0;
};

struct VOutput
{
    float4 pos: SV_Position;
    float3 test: NORMAL0;
    float2 uv:  TEXCOORD0;
};

//------------------------------------------------------------------------------------
// Vertex Shader
//------------------------------------------------------------------------------------
VOutput vs(VInput vin)
{
    VOutput vout;

    int tile_id = vin.pos.w;

    vout.pos = mul(worldviewproj, float4(vin.pos.xyz, 1));
    //vout.pos /= vout.pos.w;

    //vout.pos = mul(float4(vin.pos.xyz, 1), worldviewproj);
    vout.uv = float2(tile_id, tile_id);

    vout.test=abs(vin.pos.xyz);

    return vout;
}


//Texture<float4> tex: register(t0);

SamplerState basic_sampler: register(s0);

float4 main(VOutput pin): SV_Target0
{
//    return env_map.Sample(basic_sampler, pin.uv);
//return float4(0, pin.uv.x / 160.0, 0, 1);
return float4(pin.test.x / (128 * 128), pin.test.y / (32 * 32), pin.test.z / (128 * 128), 1);
}